from typing import *
        
from dataclasses import dataclass, field
from src.util import Input
from src.util import StringExtractor
from util import time_function


@time_function
def solve(filename: str):
    input = Input(filename).read()

    se = StringExtractor(pattern=r"(\d+)-(\d+),(\d+)-(\d+)")
    number_of_subsets: int = 0
    number_of_overlaps: int = 0
    for i in input:
        e1f, e1t, e2f, e2t = se.extract(i)

        if (int(e1f) >= int(e2f) and int(e1t) <= int(e2t)) or \
            (int(e2f) >= int(e1f) and int(e2t) <= int(e1t)):
            number_of_subsets += 1

        for j in range(int(e1f), int(e1t) + 1):
            if j >= int(e2f) and j <= int(e2t):
                number_of_overlaps += 1
                break

    print(f"Part 1: Number of complete subsets: {number_of_subsets}")
    print(f"Part 2: Number of overlapping combinations: {number_of_overlaps}")

if __name__ == '__main__':
    solve('input.txt')