from typing import *
from dataclasses import dataclass, field
from src.util import Input, GridSpace, GridAgent, Position, Velocity
from src.util import StringExtractor
from util import time_function


def define_position_tail(head: GridAgent, tail: GridAgent) -> Position:
    if head.is_touching_with(tail, allow_diagonal=True, allow_overlap=True):
        return tail.position

    


@time_function
def solve(filename: str):
    input = Input(filename).read()

    se = StringExtractor(pattern="([LURD]) (\d+)")

    space = GridSpace()
    head = GridAgent(position=Position(col=0, row=0), name='head')
    space.add_agent(head)
    tail = head.clone()
    tail.name = 'tail'
    space.add_agent(tail)

    velocities = {
        'L': Velocity(dx=-1, dy=0),
        'U': Velocity(dx=0, dy=-1),
        'R': Velocity(dx=1, dy=0),
        'D': Velocity(dx=-0, dy=1)
    }

    for command in input:
        direction, count = se.extract(command)
        head.velocity = velocities[direction]
        print(command)
        for i in range(int(count)):
            space.update()

        space.print()



    

if __name__ == '__main__':
    solve('test.txt')