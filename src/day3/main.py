from typing import *
        
from dataclasses import dataclass, field
from src.util import Input
from src.util import StringExtractor
from util import time_function


@time_function
def solve(filename: str):
    input = Input(filename).read()
    input = [i for i in input if i != ""]
    priority: str = "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    common_items: List[int] = []
    for i in input:
        if i == '':
            continue
        common = set(i[:len(i)//2]).intersection(set(i[len(i)//2:]))
        common_items.append(list(common)[0])

    print(f"Part 1: Total sum of all priorities: {sum([priority.index(i) for i in common_items])}")

    # Part 2
    common_items: List[int] = []
    for b1, b2, b3 in [input[x:x+3] for x in range(0, len(input), 3)]:
        common = set(b1).intersection(set(b2).intersection(set(b3)))
        common_items.append(list(common)[0])

    print(f"Part 2: Total sum of all priorities of badges: {sum([priority.index(i) for i in common_items])}")


if __name__ == '__main__':
    solve('input.txt')