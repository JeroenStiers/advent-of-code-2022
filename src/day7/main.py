
from typing import *        
from dataclasses import dataclass, field
from src.util import Input, DirectionalNetwork, Node, Graph
from src.util import StringExtractor
from util import time_function


class FileNode(Node):
    def __init__(self, size, **kwargs):
        self.size = size
        super().__init__(**kwargs)

    def __str__(self):
        return f"FileNode({super().__str__()}, size={self.size})"


class DirNode(Node):
    def __init__(self, **kwargs):
        self.size = 0
        super().__init__(**kwargs)

    def __str__(self):
        return f"DirNode({super().__str__()}, size={self.size})"


def parse_tree(input: List[str]):

    tree = Graph(
        network=DirectionalNetwork()
    )
    current_node: Node = tree.add_node(DirNode(name='root'))

    regex_cd = StringExtractor(pattern=r"\$ cd ([a-z0-9]+)")
    regex_dir = StringExtractor(pattern=r"dir ([a-z0-9]+)")
    regex_file = StringExtractor(pattern=r"([0-9]+) ([a-z0-9.]+)")

    for i in input:
        if name := regex_dir.extract(i):
            tree.add_connection(node1=current_node, node2=tree.add_node(DirNode(name=name[0])))
            continue

        if info := regex_file.extract(i):
            tree.add_connection(node1=current_node, node2=tree.add_node(FileNode(name=info[1], size=int(info[0]))))
            continue

        if i == '$ cd /':
            current_node = tree.visit(node='root')
            continue

        if i == '* cd ..':
            current_node = current_node.incoming[0]
            continue

        if i == '$ ls':
            continue

        if dir := regex_cd.extract(i):
            current_node = tree.visit(node=dir[0])

            if isinstance(current_node, FileNode):
                raise ValueError(f"A directory is expected but a FileNode is found ({current_node}).")
            continue
    return tree


def update_filesize_dir(tree, name_dir: str):

    filesize: int = 0
    for i in tree.visit(node=name_dir).outgoing:
        if isinstance(i, FileNode):
            filesize += i.size
            continue
        filesize += update_filesize_dir(tree, name_dir=i)
    tree.visit(node=name_dir).size = filesize
    return filesize


@time_function
def solve(filename: str):
    input = Input(filename).read()
    tree = parse_tree(input)

    total_filesize = update_filesize_dir(tree, name_dir='root')

    part1 = sum([i.size for i in tree.iterate_all_nodes() if isinstance(i, DirNode) and i.size <= 100_000])
    print(part1)


if __name__ == '__main__':
    solve('input.txt')