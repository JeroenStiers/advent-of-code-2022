from dataclasses import dataclass, field
from typing import List

from src.util import Input
from util import time_function


@dataclass
class Elf:
    snacks: List[int] = field(default_factory=list)

    def add_snack(self, calories: int):
        self.snacks.append(calories)

    def __str__(self):
        return f"Elf(number_of_snacks={self.number_of_snacks}, number_of_calories={self.number_of_calories}, snacks={self.snacks})"

    @property
    def number_of_snacks(self) -> int:
        return len(self.snacks)

    @property
    def number_of_calories(self) -> int:
        return sum(self.snacks)


@dataclass
class StarParty:
    elves: List[Elf] = field(default_factory=list)

    def add_elf(self, elf: Elf):
        if elf.number_of_snacks == 0:
            return
        self.elves.append(elf)

    def get_elves_ordered_by_calories(self, reverse: bool = True) -> List[Elf]:
        return sorted(self.elves, key=lambda e: e.number_of_calories, reverse=reverse)

    @property
    def elf_with_max_calories(self) -> Elf:
        return self.get_elves_ordered_by_calories()[0]


@time_function
def solve(filename: str):
    input = Input(filename).read(remove_emtpy_lines=False)

    star_party = StarParty()
    e = Elf()
    for i in input:
        if i == '':
            star_party.add_elf(elf=e)
            e = Elf()
            continue
        e.add_snack(calories=int(i))
    star_party.add_elf(elf=e)

    print(f"Part 1: The elf with the most calories is {star_party.elf_with_max_calories}")
    print(f"Part 2: The total number of calories caried by the 3 elves with most calories: {sum([e.number_of_calories for e in star_party.get_elves_ordered_by_calories()[:3]])}")


if __name__ == '__main__':
    solve('input.txt')
