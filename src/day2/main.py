from typing import *
from enum import Enum
from dataclasses import dataclass, field
from src.util import Input
from src.util import StringExtractor
from util import time_function, Graph, DirectionalNetwork


class HandGesture(Enum):
    ROCK = 'rock'
    PAPER = 'paper'
    SCISSORS = 'scissors'

    @property
    def score(self) -> int:
        match self:
            case HandGesture.ROCK:
                return 1
            case HandGesture.PAPER:
                return 2
            case HandGesture.SCISSORS:
                return 3


class Outcome(Enum):
    LOSE = 'lose'
    DRAW = 'draw'
    WIN = 'win'

    @property
    def score(self) -> int:
        match self:
            case Outcome.LOSE:
                return 0
            case Outcome.DRAW:
                return 3
            case Outcome.WIN:
                return 6

RULEBOOK = Graph(
    network=DirectionalNetwork()
)
## outgoing = 'beats'
## incoming = 'is beaten by'
RULEBOOK.add_connection(name1=HandGesture.ROCK, name2=HandGesture.SCISSORS)
RULEBOOK.add_connection(name1=HandGesture.SCISSORS, name2=HandGesture.PAPER)
RULEBOOK.add_connection(name1=HandGesture.PAPER, name2=HandGesture.ROCK)


@dataclass
class Turn:
    opponent: HandGesture
    you: HandGesture

    _hand_gesture_order: List[str] = field(default_factory=lambda: ['rock', 'paper', 'scissors', 'rock'])

    def outcome(self) -> Outcome:
        if self.you == self.opponent:
            return Outcome.DRAW
        if list(RULEBOOK.visit(self.you).outgoing)[0].name == self.opponent:
            return Outcome.WIN
        return Outcome.LOSE

    def calculate_score(self):
        return self.you.score + self.outcome().score


@dataclass
class StrategyGuide:
    turns: List[Turn] = field(default_factory=list)

    def add_turn(self, turn: Turn):
        self.turns.append(turn)

    def run_simulation(self) -> int:
        score: int = 0
        for t in self.turns:
            score += t.calculate_score()
        return score


def define_gesture_based_on_opponent(opponent: HandGesture, outcome: Outcome) -> HandGesture:
    match outcome:
        case Outcome.DRAW:
            return opponent
        case Outcome.WIN:
            return list(RULEBOOK.visit(opponent).incoming)[0].name
    return list(RULEBOOK.visit(opponent).outgoing)[0].name


@time_function
def solve(filename: str):
    input = Input(filename).read()
    string_extractor = StringExtractor(pattern=r"([A-C]) ([X-Z])")

    opponent_input: Dict[str, HandGesture] = {
        'A': HandGesture.ROCK,
        'B': HandGesture.PAPER,
        'C': HandGesture.SCISSORS
    }

    you_input: Dict[str, HandGesture] = {
        'X': HandGesture.ROCK,
        'Y': HandGesture.PAPER,
        'Z': HandGesture.SCISSORS
    }
    result_input: Dict[str, Outcome] = {
        'X': Outcome.LOSE,
        'Y': Outcome.DRAW,
        'Z': Outcome.WIN
    }

    strategy_guide = StrategyGuide()
    strategy_guide2 = StrategyGuide()
    for i in input:
        opponent, you = string_extractor.extract(input=i)
        strategy_guide.add_turn(Turn(
            you=you_input[you],
            opponent=opponent_input[opponent]
        ))
        strategy_guide2.add_turn(Turn(
            opponent=opponent_input[opponent],
            you=define_gesture_based_on_opponent(
                opponent=opponent_input[opponent],
                outcome=result_input[you]
            )
        ))

    print(f"Part 1: The total score following the strategy_guide is {strategy_guide.run_simulation()}")
    print(f"Part 2: The total score following the strategy_guide is {strategy_guide2.run_simulation()}")


if __name__ == '__main__':
    solve('input.txt')