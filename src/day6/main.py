from typing import *
from dataclasses import dataclass, field
from src.util import Input
from src.util import StringExtractor
from util import time_function


@time_function
def solve(filename: str):
    input = Input(filename).read()[0]

    for i in range(len(input)):
        if len(set(input[i:i+4])) == 4:
            break
    print(i+4)
    for i in range(len(input)):
        if len(set(input[i:i+14])) == 14:
            break
    print(i+14)
    

if __name__ == '__main__':
    solve('input.txt')