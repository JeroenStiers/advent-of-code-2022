from typing import *
from dataclasses import dataclass, field
from src.util import Input, NumericalGrid, Position
from util import time_function


def define_viewable_indices(heights: List[int]) -> Tuple[List[int], List[int]]:
    current_height: int = heights[0]

    # Initialise with 1 since the outer tree is always visible
    result: List[int] = [1]
    result_equal: list[int] = [0]
    for i in heights[1:]:
        if i >= current_height:
            result_equal.append(1)
            if i > current_height:
                result.append(1)
            current_height = i
            continue
        result.append(0)
        result_equal.append(0)
    return result, result_equal


def define_viewshed(grid: NumericalGrid):
    viewshed: NumericalGrid = NumericalGrid.initialise(width=grid.width, height=grid.height, value=0)

    # left
    for row in grid.get_row_indices():
        for i, viewable in enumerate(define_viewable_indices(list(grid.get_row(row)))[0]):
            viewshed.grid[row][i] += viewable

    # right
    for row in grid.get_row_indices():
        for i, viewable in enumerate(define_viewable_indices(list(grid.get_row(row))[::-1])[0]):
            viewshed.grid[row][grid.width - i - 1] += viewable

    # top
    for col in grid.get_col_indices():
        for i, viewable in enumerate(define_viewable_indices(list(grid.get_col(col)))[0]):
            viewshed.grid[i][col] += viewable

    # bottom
    for col in grid.get_col_indices():
        for i, viewable in enumerate(define_viewable_indices(list(grid.get_col(col))[::-1])[0]):
            viewshed.grid[grid.height - i - 1][col] += viewable

    return viewshed


def calculate_scenic_score_position(left: List[int], top: List[int], right: List[int], bottom: List[int]) -> int:

    if any([len(left) == 0, len(top) == 0, len(right) == 0, len(bottom) == 0]):
        return 0

    score: int = 1

    # left
    try:
        tree_visibility = define_viewable_indices(left)[1]
        score *= tree_visibility.index(1)
    except ValueError:
        score *= len(tree_visibility)-1

    # top
    try:
        tree_visibility = define_viewable_indices(top)[1]
        score *= tree_visibility.index(1)
    except ValueError:
        score *= len(tree_visibility)-1

    # right
    try:
        tree_visibility = define_viewable_indices(right)[1]
        score *= tree_visibility.index(1)
    except ValueError:
        score *= len(tree_visibility)-1

    # bottom
    try:
        tree_visibility = define_viewable_indices(bottom)[1]
        score *= tree_visibility.index(1)
    except ValueError:
        score *= len(tree_visibility)-1

    return score


def define_scenic_scores(grid: NumericalGrid) -> NumericalGrid:
    scores: NumericalGrid = NumericalGrid.initialise(width=grid.width, height=grid.height, value=0)

    for pos in grid.get_positions():
        # For each position in the grid, calculate the scenic score in all directions
        scores.set_value(positions=[pos], value=calculate_scenic_score_position(
            left=list(grid.get_row(pos.row))[0:pos.col+1][::-1],
            top=list(grid.get_col(pos.col))[0:pos.row+1][::-1],
            right=list(grid.get_row(pos.row))[pos.col:],
            bottom=list(grid.get_col(pos.col))[pos.row:]
        ))

    return scores


@time_function
def solve(filename: str):
    input = Input(filename).read()

    # Creation of the grid
    grid = NumericalGrid()
    for i in input:
        grid.add_row(row=map(int, i))

    # Analyse viewshed from each direction
    viewshed = define_viewshed(grid)

    count_visible_trees = sum([1 for i in viewshed.get_values() if i > 0])
    print(f"Part 1: The number of visible trees is {count_visible_trees}")

    # Calculate the scenic score
    scenic_scores = define_scenic_scores(grid)
    max_scenic_score = max(scenic_scores.get_values())
    print(f"Part 2: The max scenic score is {max_scenic_score}")
    

if __name__ == '__main__':
    solve('input.txt')