from typing import *
from math import floor
from copy import deepcopy
from dataclasses import dataclass, field
from src.util import Input
from src.util import StringExtractor
from util import time_function


def interpret_initial_state(input: List[str]) -> List[List[str]]:

    se = StringExtractor(pattern=r"\[([A-Z])\]")
    state: List[List[str]] = [[] for _ in range(25)]
    for line in input:
        if line == '':

            # Reverse each of the lists so that the box on top is the last
            # and remove the empty positions
            return [i[::-1] for i in state if len(i) > 0]

        for box in se.finditer(line):
            pos_char = box.start() + 1
            pile = floor(pos_char / 4)

            if len(state) < pile:
                raise ValueError('The initialised state is not sufficiently big.')
            state[pile].append(box.group(1))


def print_state(state):
    # Print the state visually to allow for easier debugging
    # start with creating a deepcopy of the list since this is a mutable object in Python
    state = deepcopy(state)

    print(''.join([f" {i+1}  " for i in range(len(state))]))
    print(''.join(['----' for _ in range(len(state))]))
    while(sum([len(i) for i in state])) > 0:
        print(''.join([f"[{i.pop()}] " if len(i) > 0 else '    ' for i in state]))
    print()


def operate_crane(state: List[List[str]], operations: List[str]):

    cratemover_9000 = deepcopy(state)
    cratemover_9001 = deepcopy(state)

    se = StringExtractor(pattern=r"move (\d+) from (\d+) to (\d+)")
    for operation in operations:
        try:
            count, from_pile, to_pile = se.extract(operation)

            print_state(cratemover_9001)
            print(f"Moving {count} from {from_pile} to {to_pile}")
            print()

            # 9000
            for move in range(int(count)):
                cratemover_9000[int(to_pile)-1].append(cratemover_9000[int(from_pile)-1].pop())

            # 9001
            cratemover_9001[int(to_pile)-1].extend(cratemover_9001[int(from_pile)-1][-int(count):])
            cratemover_9001[int(from_pile)-1] = cratemover_9001[int(from_pile)-1][:-int(count)]


        except TypeError as e:
            # Whenever we encounter a line that does not contain 'move' information
            continue
    return cratemover_9000, cratemover_9001

@time_function
def solve(filename: str):
    input = Input(filename).read(remove_emtpy_lines=False)
    state = interpret_initial_state(input)

    state_9000, state_9001 = operate_crane(state, input)
    print(f"Part 1: The boxes on top of each of the piles are {''.join([i[-1] for i in state_9000 if len(i) > 0])}")
    print(f"Part 2: The boxes on top of each of the piles are {''.join([i[-1] for i in state_9001 if len(i) > 0])}")


if __name__ == '__main__':
    solve('input.txt')